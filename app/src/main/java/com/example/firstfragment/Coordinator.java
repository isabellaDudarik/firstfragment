package com.example.firstfragment;

public interface Coordinator {
    void showNewFragment(String text);
}
