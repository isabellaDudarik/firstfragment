package com.example.firstfragment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.FrameLayout;

public class MainActivity extends AppCompatActivity implements Coordinator {


    FrameLayout container;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        container = findViewById(R.id.root);

        if (savedInstanceState == null) {//if first launch
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                initPortrait();
            } else {
                initLandscape();
            }

        } else {
            //restore state
        }
    }

    private void initPortrait() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.root, new ButtonFragment())
                .commit();
    }

    private void initLandscape() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.root, new ButtonFragment())
                .commit();

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.root_right, TextFragment.newInstance(""))
                .commit();

    }


    @Override
    public void showNewFragment(String text) {
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.root, TextFragment.newInstance(text))
                    .addToBackStack(null)
                    .commit();
        } else {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.root_right, TextFragment.newInstance(text))
                    .commit();

        }
    }
}
