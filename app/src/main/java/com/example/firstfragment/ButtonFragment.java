package com.example.firstfragment;


import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class ButtonFragment extends Fragment {
    Coordinator coordinator;
    EditText editText;
    Button action;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Coordinator) {
            coordinator = (Coordinator) context;
        } else {
            throw new IllegalArgumentException(context.getClass().getSimpleName() + " must implemented interface Coordinator");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_right, container, false);
        editText = root.findViewById(R.id.edit_text);
        action = root.findViewById(R.id.button);
        action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                coordinator.showNewFragment(editText.getText().toString());
            }
        });
        return root;
    }
        /*2 way*/
//    public void setCoordinator(Coordinator coordinator) {
//        this.coordinator = coordinator;
//    }

}
